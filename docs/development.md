
## Create Express configurations and the questions to populate (server side)
1. Define the body for Express to parse into Json; declaring variables of express and bodyParser
2. Configure the Node Port for console log
3. Declare the questions (quizes)
4. Creating the Get function and randomize the questions to be called upon; using the Math.random and Math.floor JS functions
5. Creating the Post function (stringify and parse into Json) when the Quiz is answered and defining the functions when called upon; if statement when correct and else statement when incorrect
6. Creating the Use function for fallback (ie page not found)
7. Creating the Listen node


## Create the HTML and Client side modules
1. Create the basic HTML structure
2. Link the dependencies/ libraries
3. Initialize the app Directive within the html document
4. Create the main Controller class and define its Angular controller directive 
5. In the client JS file, create the module popQuizApp. Then create the popQuiz controller definition for module and its dependency list
6. In the html structure, for the popQuiz (questions) class, create the controller module definition and its dependency list. Create the two way binding attribute with the controllers
7. In the client JS file, define the functions when; the Quiz question is called and when the Quiz is answered.  
8. Initialize the Form function and the actions that will be be binded to. This will return back to the server side for action; the random question that will be shown and its corresponding answer. The endpoint for this will be back to app.get in the server side. 
9. In the html file, create the Answers model directive and the definitions it will have. Then include the value inputs that it will be populated with and create the two binding attribute with these values. 
10. Create and define the Comments input model directive and its functions that will be binded to it
11. Create the button/ submit id class in the html. Also create the two way binding attribute 
12. Upon submit btn-class action, the ctrl.finalanswer.message will lead back to endpoint of app.post on the server side.  


