## Prepare project popQuiz structure
1. mkdir <project popQuiz>
1. mkdir <project popQuiz>/server
1. mkdir <project popQuiz>/client

## Setup Bitbucket
1. Create new repo on Bitbucket
1. Choose public or private repo access level

## Prepare Git
1. git init
1. Create a git ignore file .gitignore e.g. node_modules
1. git remote -v  (check on the current directory which remote is configured to)
1. To remove the git origin remote ( git remote remove origin)
1. add remote origin , git remote add origin <http git url>. e.g. git remote add origin https://munism13@bitbucket.org/munism13/popquiz.git

## Global Utility installation
1. npm install -g nodemon
1. npm install -g bower

## Prepare Express JS
1. npm init
1. npm install express --save

## How to start my app
1. Mac - export NODE_PORT=3000 or Windows - set NODE_PORT=3000
1. nodemon or nodemon server/app.js

## How to test my app
1. Launch Chrome , point to http://localhost:3000 or https://arcane-caverns-83264.herokuapp.com/


## Create Angular Module (client side)
1. In the client side, create the module popQuizApp 
2. Then, create popQuiz controller definition for module and its dependency list
3. Create the functions for popQuizCtrl
4. Within the function, define the values for the Quiz question and the Quiz answer
5. Define the functions when; the Quiz question is called and when the Quiz is answered

## Create Express configurations and the questions to populate (server side)
1. Define the body for Express to parse into Json; declaring variables of express and bodyParser
2. Configure the Node Port for console log
3. Declare the questions (quizes)
4. Creating the Get function and randomize the questions to be called upon; using the Math.random and Math.floor JS functions
5. Creating the Post function (stringify and parse into Json) when the Quiz is answered and defining the functions when called upon; if statement when correct and else statement when incorrect
6. Creating the Use function for fallback (ie page not found)
7. Creating the Listen node

## Create the HTML 
1. Initialize the app Directive 
2. Link the dependencies/ libraries
3. Create the class and define the Angular controller directive 
4. Create the submit directive within the same class
5. Create the two way binding attribute with the controllers 
6. For the Answers input elements, include the model definitions and its values to which it will be populated with
7. For the Comments input elements, include the model definitions and its values to which it will be populated with 
8. For the button input element, define the functions that will be called on; controller module, the final answer and its corresponding message

